const API_URL = process.env.REACT_APP_API_URL;
const TOP_NEWS_URL = 'top-headlines';

export async function getNews(apiKey: string, query?: string) {
	const url = new URL(`${API_URL}/${TOP_NEWS_URL}`);
	url.searchParams.append('language', 'en');
	url.searchParams.append('apiKey', apiKey);
	query && url.searchParams.append('q', query);

	const response = await fetch(url.toString());

	const data = await response.json();

	if (response.status >= 200 && response.status < 300) {
		return data;
	} else {
		throw new Error(data.message);
	}
}
