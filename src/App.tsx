import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Login from './pages/Login';
import Main from './pages/Main';
import NotFound from './pages/NotFound';
import Profile from './pages/Profile';
import Story from './pages/Story';
import Layout from './components/shared/Layout';
import ProtectedRoute from './components/shared/ProtectedRoute';
import { useAppSelector } from './hooks/reduxHooks';
import { selectIsAuthenticated } from './store/authSlice';

function App() {
	const isAuthenticated = useAppSelector(selectIsAuthenticated);

	return (
		<BrowserRouter>
			<Routes>
				<Route path='/' element={<Layout />}>
					<Route path='/login' element={<Login />} />
					<Route element={<ProtectedRoute isAuthenticated={isAuthenticated} />}>
						<Route index element={<Main />} />
						<Route path='/article/:articleTitle' element={<Story />} />
						<Route path='/profile' element={<Profile />} />
					</Route>
					<Route path='*' element={<NotFound />} />
				</Route>
			</Routes>
		</BrowserRouter>
	);
}

export default App;
