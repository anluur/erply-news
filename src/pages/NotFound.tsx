import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

function NotFound() {
	return (
		<Container>
			<Row className='justify-content-center'>
				<Col className='text-center my-5'>
					<h3>Page not found</h3>
				</Col>
			</Row>
		</Container>
	);
}

export default NotFound;
