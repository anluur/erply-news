import React from 'react';
import {
	Alert,
	Col,
	Container,
	FormControl,
	InputGroup,
	Row,
	Button,
	Spinner,
} from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from '../hooks/reduxHooks';
import {
	clear,
	fetchNews,
	selectError,
	selectIsLoading,
	selectNewsList,
} from '../store/newsSlice';
import ArticleCard from '../components/ArticleCard';

function Main() {
	const dispatch = useAppDispatch();
	const articles = useAppSelector(selectNewsList);
	const error = useAppSelector(selectError);
	const isLoading = useAppSelector(selectIsLoading);
	const [query, setQuery] = React.useState('');

	const handleSearch = (event: React.ChangeEvent<HTMLFormElement>) => {
		event.preventDefault();
		dispatch(fetchNews({ query }));
	};

	React.useEffect(() => {
		dispatch(fetchNews({}));
		return () => {
			dispatch(clear());
		};
	}, [dispatch]);

	return (
		<Container fluid>
			<Row>
				<Col className='my-4'>
					<h3>Top headlines</h3>
					<form className='mt-3' onSubmit={handleSearch}>
						<InputGroup className='shadow'>
							<FormControl
								type='text'
								value={query}
								onChange={(event) => setQuery(event.target.value)}
							/>
							<InputGroup.Append>
								<Button type='submit'>Search</Button>
							</InputGroup.Append>
						</InputGroup>
					</form>
				</Col>
			</Row>
			<Row>
				{isLoading && (
					<Col className='d-flex justify-content-center'>
						<Spinner animation='border' />
					</Col>
				)}
				{!isLoading && error && (
					<Col>
						<Alert variant='danger'>{error}</Alert>
					</Col>
				)}
				{!isLoading &&
					!error &&
					articles?.map((article, idx) => (
						<Col
							md={6}
							lg={4}
							xl={3}
							className='py-3'
							key={idx + article.title}
						>
							<ArticleCard article={article} />
						</Col>
					))}
			</Row>
		</Container>
	);
}

export default Main;
