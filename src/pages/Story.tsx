import React from 'react';
import { Col, Container, Row, Spinner, Alert } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import ArticleDetails from '../components/ArticleDetails';
import { useAppDispatch, useAppSelector } from '../hooks/reduxHooks';
import {
	clear,
	fetchNews,
	selectError,
	selectIsLoading,
	selectNewsList,
} from '../store/newsSlice';

function Story() {
	const { articleTitle } = useParams();

	const dispatch = useAppDispatch();
	const articles = useAppSelector(selectNewsList);
	const error = useAppSelector(selectError);
	const isLoading = useAppSelector(selectIsLoading);

	React.useEffect(() => {
		dispatch(fetchNews({ query: articleTitle }));
		return () => {
			dispatch(clear());
		};
	}, [dispatch, articleTitle]);

	return (
		<Container>
			<Row className='justify-content-center'>
				{isLoading && (
					<Col className='d-flex justify-content-center mt-4'>
						<Spinner animation='border' />
					</Col>
				)}
				{!isLoading && error && (
					<Col className='mt-4'>
						<Alert variant='danger'>{error}</Alert>
					</Col>
				)}
				{!isLoading && !error && (
					<Col>
						{articles[0] ? (
							<ArticleDetails article={articles[0]} />
						) : (
							<h3 className='text-center my-5'>Article not found</h3>
						)}
					</Col>
				)}
			</Row>
		</Container>
	);
}

export default Story;
