import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import LoginForm from '../components/LoginForm';
import { login } from '../store/authSlice';

function Login() {
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const handleLogin = async (credentials: Credentials) => {
		await dispatch(login(credentials));
		navigate('/');
	};

	return (
		<Container>
			<Row className='justify-content-center'>
				<Col lg={7} xl={6} className='my-5'>
					<LoginForm onSubmit={handleLogin} />
				</Col>
			</Row>
		</Container>
	);
}

export default Login;
