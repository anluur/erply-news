import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import UserInfoForm from '../components/UserInfoForm';
import { useAppDispatch } from '../hooks/reduxHooks';
import { updateUserProfile } from '../store/authSlice';

function Profile() {
	const dispatch = useAppDispatch();

	const handleSave = async (userInfo: UserInfo) => {
		await dispatch(updateUserProfile(userInfo));
	};

	return (
		<Container>
			<Row className='justify-content-center'>
				<Col lg={7} xl={6} className='my-5'>
					<UserInfoForm onSubmit={handleSave} />
				</Col>
			</Row>
		</Container>
	);
}

export default Profile;
