import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { updateProfile } from '../api/profileApi';
import { RootState } from '../store';

interface AuthState {
	isAuthenticated: boolean;
	user: {
		name: string | null;
		email: string | null;
		apiKey: string | null;
	};
	isLoading: boolean;
}

const initialState: AuthState = {
	isAuthenticated: false,
	user: {
		name: null,
		email: null,
		apiKey: null,
	},
	isLoading: false,
};

export const updateUserProfile = createAsyncThunk(
	'auth/updateProfile',
	async (userInfo: UserInfo) => {
		return await updateProfile(userInfo);
	}
);

export const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		login: (state, action: PayloadAction<Credentials>) => {
			state.user.name = '';
			state.user.email = action.payload.email;
			state.user.apiKey = action.payload.apiKey;
			state.isAuthenticated = true;
		},
		logout: (state) => {
			state.user.name = null;
			state.user.email = null;
			state.user.apiKey = null;
			state.isAuthenticated = false;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(updateUserProfile.pending, (state) => {
			state.isLoading = true;
		});
		builder.addCase(updateUserProfile.fulfilled, (state, { payload }) => {
			const { name, email, apiKey } = payload as UserInfo;
			state.isLoading = false;
			state.user.name = name;
			state.user.email = email;
			state.user.apiKey = apiKey;
		});
	},
});

export default authSlice.reducer;

export const { login, logout } = authSlice.actions;

export const selectCredentials = (state: RootState) => state.auth.user;
export const selectIsAuthenticated = (state: RootState) =>
	state.auth.isAuthenticated;
export const selectIsLoading = (state: RootState) => state.auth.isLoading;
