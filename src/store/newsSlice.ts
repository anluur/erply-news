import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '.';
import { getNews } from '../api/newsApi';
import { selectCredentials } from './authSlice';

interface NewsState {
	articles: Article[];
	error: string | null;
	isLoading: boolean;
}

const initialState: NewsState = {
	articles: [],
	error: null,
	isLoading: false,
};

export const fetchNews = createAsyncThunk<
	NewsResponse,
	{ query?: string },
	{
		rejectValue: string;
	}
>('news/fetchNews', async ({ query }, { getState, rejectWithValue }) => {
	try {
		return await getNews(
			selectCredentials(getState() as RootState).apiKey ?? '',
			query
		);
	} catch (error) {
		return rejectWithValue((error as Error).message);
	}
});

export const newsSlice = createSlice({
	name: 'news',
	initialState,
	reducers: {
		clear: (state) => {
			state.articles = [];
			state.error = null;
			state.isLoading = false;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchNews.pending, (state) => {
			state.isLoading = true;
		});
		builder.addCase(fetchNews.rejected, (state, { payload }) => {
			state.isLoading = false;
			state.error = payload as string;
		});
		builder.addCase(fetchNews.fulfilled, (state, { payload }) => {
			const { articles } = payload as NewsResponse;
			state.isLoading = false;
			state.articles = articles;
		});
	},
});

export default newsSlice.reducer;

export const { clear } = newsSlice.actions;

export const selectNewsList = (state: RootState) => state.news.articles;
export const selectIsLoading = (state: RootState) => state.news.isLoading;
export const selectError = (state: RootState) => state.news.error;
