import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Nav, Navbar } from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { logout, selectIsAuthenticated } from '../../store/authSlice';

function Header() {
	const isAuthenticated = useAppSelector(selectIsAuthenticated);
	const dispatch = useAppDispatch();

	const handleLogout = () => {
		dispatch(logout());
	};
	return (
		<Navbar bg='dark' variant='dark' className='w-100'>
			<Navbar.Brand>News</Navbar.Brand>
			<Nav className='mr-auto'>
				{isAuthenticated && (
					<>
						<Link to='/' className='nav-link'>
							Home
						</Link>
						<Link to='/profile' className='nav-link'>
							My profile
						</Link>
					</>
				)}
			</Nav>
			{isAuthenticated ? (
				<Button variant='outline-light' onClick={handleLogout}>
					Log out
				</Button>
			) : (
				<Link to='/login'>
					<Button variant='outline-light'>Log in</Button>
				</Link>
			)}
		</Navbar>
	);
}

export default Header;
