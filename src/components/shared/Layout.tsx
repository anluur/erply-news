import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';
import Header from './Header';

function Layout() {
	return (
		<Container fluid>
			<Row>
				<Header />
			</Row>
			<Row>
				<Col>
					<Outlet />
				</Col>
			</Row>
		</Container>
	);
}

export default Layout;
