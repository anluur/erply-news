import React from 'react';
import { Outlet, Navigate } from 'react-router-dom';

interface Props {
	isAuthenticated: boolean;
}

function ProtectedRoute({ isAuthenticated }: Props) {
	return isAuthenticated ? <Outlet /> : <Navigate to='/login' replace />;
}

export default ProtectedRoute;
