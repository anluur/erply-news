import React from 'react';
import { Formik, Form, Field, FieldProps } from 'formik';
import { FormControl, FormLabel, Button } from 'react-bootstrap';

interface Props {
	onSubmit(credentials: Credentials): void;
}

function LoginForm({ onSubmit }: Props) {
	const initialValues = {
		email: '',
		apiKey: '',
	};

	return (
		<Formik
			initialValues={initialValues}
			onSubmit={(values) => {
				onSubmit(values);
			}}
		>
			<Form className='border rounded shadow p-4'>
				<h4>Login</h4>
				<div className='mt-3'>
					<FormLabel htmlFor='email'>Email</FormLabel>
					<Field name='email'>
						{({ field }: FieldProps) => (
							<FormControl {...field} type='email' required />
						)}
					</Field>
				</div>
				<div className='mt-3'>
					<FormLabel htmlFor='apiKey'>API key</FormLabel>
					<Field name='apiKey'>
						{({ field }: FieldProps) => (
							<FormControl {...field} type='text' required />
						)}
					</Field>
				</div>
				<Button type='submit' className='btn-block mt-4'>
					Log in
				</Button>
				<p className='mt-3 mb-0'>
					Don't have an API key? Get one{' '}
					<a
						href='https://newsapi.org/register'
						target='_blank'
						rel='noreferrer'
					>
						here
					</a>
					.
				</p>
			</Form>
		</Formik>
	);
}

export default LoginForm;
