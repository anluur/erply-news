import React from 'react';
import { Link } from 'react-router-dom';

interface Props {
	article: Article;
}

function ArticleDetails({ article }: Props) {
	return (
		<div>
			<h3 className='font-weight-bold mt-3'>{article.title}</h3>
			{article.urlToImage && (
				<Link to={`/article/${article.title}`} className='big-img-container'>
					<img src={article.urlToImage} alt='Article illustration not found' />
				</Link>
			)}
			<div>
				<p>Author: {article.author}</p>
				<p>Published: {new Date(article.publishedAt).toLocaleString()}</p>
			</div>
			<hr />
			<div>
				<h5>{article.description}</h5>
				<p className='mt-3'>{article.content}</p>
				<p className='mt-3'>
					Source:{' '}
					<a href={article.url} target='_blank' rel='noreferrer'>
						{article.source.name}
					</a>
				</p>
			</div>
		</div>
	);
}

export default ArticleDetails;
