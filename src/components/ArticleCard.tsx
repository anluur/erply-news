import React from 'react';
import { Link } from 'react-router-dom';
import { Card } from 'react-bootstrap';

interface Props {
	article: Article;
}

function ArticleCard({ article }: Props) {
	return (
		<Card className='shadow h-100'>
			{article.urlToImage && (
				<Link to={`/article/${article.title}`} className='img-container'>
					<img src={article.urlToImage} alt='Article illustration not found' />
				</Link>
			)}
			<Card.Body>
				<Card.Title>
					<Link
						to={`/article/${article.title}`}
						className='text-dark text-decoration-none'
					>
						{article.title}
					</Link>
				</Card.Title>
				<Card.Text>{article.description}</Card.Text>
			</Card.Body>
		</Card>
	);
}

export default ArticleCard;
