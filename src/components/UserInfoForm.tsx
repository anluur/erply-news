import React from 'react';
import { Formik, Form, Field, FieldProps } from 'formik';
import { FormControl, FormLabel, Button, Spinner } from 'react-bootstrap';
import { useAppSelector } from '../hooks/reduxHooks';
import { selectCredentials, selectIsLoading } from '../store/authSlice';
interface Props {
	onSubmit(userInfo: UserInfo): void;
}

function UserInfoForm({ onSubmit }: Props) {
	const { name, email, apiKey } = useAppSelector(selectCredentials);
	const isLoading = useAppSelector(selectIsLoading);

	const initialValues = {
		name: name ?? '',
		email: email ?? '',
		apiKey: apiKey ?? '',
	};

	const [disabled, setDisabled] = React.useState(true);

	return (
		<Formik
			enableReinitialize
			initialValues={initialValues}
			onSubmit={(values) => {
				onSubmit(values);
				setDisabled(true);
			}}
		>
			{({ resetForm, isSubmitting }) => (
				<Form className='border rounded shadow p-4'>
					<div className='d-flex justify-content-between align-items-center'>
						<h4>My profile</h4>
						<Button
							variant='link'
							type='button'
							disabled={isSubmitting}
							onClick={() => {
								setDisabled(!disabled);
								resetForm();
							}}
						>
							{disabled ? 'Edit' : 'Cancel'}
						</Button>
					</div>
					<div className='mt-3'>
						<FormLabel htmlFor='email'>Name</FormLabel>
						<Field name='name'>
							{({ field }: FieldProps) => (
								<FormControl
									{...field}
									type='text'
									required
									disabled={disabled}
								/>
							)}
						</Field>
					</div>
					<div className='mt-3'>
						<FormLabel htmlFor='email'>Email</FormLabel>
						<Field name='email'>
							{({ field }: FieldProps) => (
								<FormControl
									{...field}
									type='email'
									required
									disabled={disabled}
								/>
							)}
						</Field>
					</div>
					<div className='mt-3'>
						<FormLabel htmlFor='apiKey'>API key</FormLabel>
						<Field name='apiKey'>
							{({ field }: FieldProps) => (
								<FormControl
									{...field}
									type='text'
									required
									disabled={disabled}
								/>
							)}
						</Field>
					</div>
					<div className='d-flex justify-content-end'>
						<Button type='submit' className='btn mt-4' disabled={disabled}>
							{isLoading ? (
								<span>
									Loading{' '}
									<Spinner
										as='span'
										animation='border'
										size='sm'
										role='status'
										aria-hidden='true'
									/>
								</span>
							) : (
								'Save'
							)}
						</Button>
					</div>
				</Form>
			)}
		</Formik>
	);
}

export default UserInfoForm;
