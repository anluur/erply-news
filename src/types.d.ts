interface Article {
	source: {
		id: string | null;
		name: string;
	};
	author: string | null;
	title: string;
	description: string;
	url: string;
	urlToImage: string;
	publishedAt: string;
	content: string;
}

interface NewsResponse {
	status: string;
	totalResults: number;
	articles: Article[];
}

interface Credentials {
	email: string;
	apiKey: string;
}

type UserInfo = Credentials & {
	name: string;
};
